FROM alpine:latest

ENV APP_PORT=''
ENV EDIT_TOKEN=''
ENV READ_TOKEN=''
ENV RECORD_NAME=''
ENV ZONE_NAME=''
ENV ADMIN_USER=''
ENV ADMIN_PASS=''

COPY ./cloudflare_dns_updater.py /

RUN apk update && apk add py3-requests py3-pip py3-flask && \
    chmod +x /cloudflare_dns_updater.py

ENTRYPOINT ["/cloudflare_dns_updater.py"]
