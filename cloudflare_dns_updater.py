#!/usr/bin/env python3

# Based on https://valh.io/p/python-script-for-cloudflare-dns-record-updates-dyndns/

from flask import Flask, request, jsonify
from functools import wraps

import json
import re
import requests
import sys
import os
import logging
import ipaddress

def make_request(kind, url, headers=None, data=None, exit_on_fail=False):
    response = ''
    if kind == 'get':
        response = requests.get(url, headers=headers, data=data)
    elif kind == 'put':
        response = requests.put(url, headers=headers, data=data)

    if response.status_code == 200:
        return True, response
    else:
        logging.critical(f'{kind} request failed: {url}\n{response.content}')
        if exit_on_fail:
            sys.exit(1)
        return False, response


def check_ip(current_ip_address):
    try:
        with open(f'cloudflare_update_record_ip4.txt', 'r', encoding='UTF-8') as f:
            old_ip = f.read()
        if current_ip_address == old_ip:
            return False
        else:
            return True
    except FileNotFoundError:
        logging.info(f'Could not find file cloudflare_update_record_ip4.txt, continuing...')
        return True


def get_identifiers():
    request_successful, zone_id_response = make_request('get', f'https://api.cloudflare.com/client/v4/zones?name={ZONE_NAME}', headers={"Authorization": f"Bearer {READ_TOKEN}", "Content-Type": "application/json"}, exit_on_fail=True)
    zone_identifier = zone_id_response.json()['result'][0]['id']

    request_successful, record_id_response = make_request('get', f'https://api.cloudflare.com/client/v4/zones/{zone_identifier}/dns_records?name={RECORD_NAME}.{ZONE_NAME}&type=A', headers={"Authorization": f"Bearer {READ_TOKEN}", "Content-Type": "application/json"}, exit_on_fail=True)


    try:
        record_identifier = record_id_response.json()['result'][0]['id']
        record_ip = record_id_response.json()['result'][0]['content']
    except IndexError:
        logging.exception(f'Could not find id of DNS record. "Results" in API response were empty. Please make sure that the specified DNS record already exists and config is correct. Try again after. Dumping exception...')
        sys.exit(1)
    return zone_identifier, record_identifier, record_ip


def update_record(ip, zone_identifier, record_identifier):
    request_successful, response = make_request('put', f'https://api.cloudflare.com/client/v4/zones/{zone_identifier}/dns_records/{record_identifier}', headers={"Authorization": f"Bearer {EDIT_TOKEN}", "Content-Type": "application/json"}, data=f'{{"id": "{zone_identifier}", "type": "A", "name": "{RECORD_NAME}","content": "{ip}"}}')

    if request_successful:
        logging.info(f'DNS A record update succeeded, IP changed to: "{ip}"')
    else:
        logging.critical(f'DNS A record update failed, dumping API response:\n{response.content}')
        sys.exit(1)


def write_ip(ip):
    logging.debug(f'Writing IPv4 address to file: {ip}')
    with open(f'cloudflare_update_record_ip4.txt', 'w', encoding='UTF-8') as f:
        f.write(ip)

def validate_ip_address(ip_string):
   try:
       ip_object = ipaddress.ip_address(ip_string)
       return True
   except ValueError:
       return False

def check_auth(username, password):
    return username == ADMIN_USER and password == ADMIN_PASS

def login_required(f):
    """ basic auth for ftp-api """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return jsonify({'message': 'Authentication required'}), 401
        return f(*args, **kwargs)
    return decorated_function

def main(current_ip_address):
    if current_ip_address:
            zone_identifier, record_identifier, record_ip = get_identifiers()
            if current_ip_address != record_ip:
                update_record(current_ip_address, zone_identifier, record_identifier)
                write_ip(current_ip_address)
                print(f'DNS record "{RECORD_NAME}" in zone "{ZONE_NAME}" updated, Current IPv4 address "{current_ip_address}"')
            elif current_ip_address == record_ip:
                update_record(current_ip_address, zone_identifier, record_identifier)
                write_ip(current_ip_address)
                print(f'DNS record "{RECORD_NAME}" in zone "{ZONE_NAME}" updated, Current IPv4 address "{current_ip_address}"')
            else:
                print(f'Current IPv4 address "{current_ip_address}" is equal to IP of DNS record "{RECORD_NAME}" in zone "{ZONE_NAME}" already: "{record_ip}". Exiting...')


APP_PORT    = os.environ['APP_PORT']
READ_TOKEN  = os.environ.get('READ_TOKEN')
EDIT_TOKEN  = os.environ.get('EDIT_TOKEN')
ZONE_NAME   = os.environ.get('ZONE_NAME')
RECORD_NAME = os.environ.get('RECORD_NAME')
ADMIN_USER  = os.environ.get('ADMIN_USER')
ADMIN_PASS  = os.environ.get('ADMIN_PASS')

app = Flask(__name__)

@app.route('/')
@login_required

def index():
    ip = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    ip = ip.split(',')[0]
    if validate_ip_address(ip):
      main(ip)
      return 'OK'
    else:
      return 'NOT OK'

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=APP_PORT)
