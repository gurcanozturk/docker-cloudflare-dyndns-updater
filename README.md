# docker-cloudflare-dynDNS-updater

Updates and set specific DNS A record value to client-IP address on Cloudflare DNS through API. Build, edit docker-compose.yml and run.

# to build
docker build -t cloudflare-dns-updater .

# to run
docker-compose up
